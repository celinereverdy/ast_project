# AST Project 2017

*NodeJS project for the Asynchronous Server Technologies course.*

## Install dependencies

Clone this repo and run `npm install`

## Populate the database

Use `npm run populateDB`

You will then be able to login as foo (password: foo123) or bar (password: bar123).

## Run

Use `npm start`

## Test

Use `npm test`

*Warning: this will erase the content of the database, you should run `npm run populateDB` afterwards.*

## Contributors

* Céline REVERDY
* Rina ATTIEH
