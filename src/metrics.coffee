module.exports = (db) ->
  # get(id, callback)
  # Get metrics
  # - id: metric's id
  # - callback: the callback function
   get: (id, callback) ->
     if callback == undefined
       callback = id
       callback new Error "missing one parameter"
     else
       result = [] #array of results to return
       rs = db.createReadStream({gte:"metrics:#{id}:", lte:"metrics:#{id}:\xff"}) #stream only metrics with right id
       rs.on 'error', (err) ->
         callback err
       rs.on 'data', (data) ->
         [dataType, dataId, dataTimestamp] = data.key.split ":"
         result.push
           id: dataId
           timestamp: dataTimestamp
           value: data.value
       rs.on 'close', () ->
         console.log "rendered metrics"
       rs.on 'end', () ->
         callback null, result

  # save(id, metrics, callback)
  # Save given metrics
  # - id: metric id
  # - metrics: an array of { timestamp, value }
  # - callback: the callback function
   save: (id, metrics, callback) ->
     if callback == undefined
       if metrics == undefined
         callback = id
         callback new Error "missing two parameters"
       else
         callback = metrics
         callback new Error "missing one parameter"
     else
       ws = db.createWriteStream()
       ws.on 'error', callback
       ws.on 'close', callback
       for metric in metrics
         { timestamp, value } =  metric
         ws.write
           key: "metrics:#{id}:#{timestamp}"
           value: value
       ws.end()

  # remove(id, timestamp, callback)
  # Remove user
  # - id: metric's id
  # - timestamp: timestamp of the metric to remove
  # - callback: the callback function
   remove: (id, timestamp, callback) ->
     if callback == undefined
       if timestamp == undefined
         callback = id
         callback new Error "missing two parameters"
       else
         callback = timestamp
         callback new Error "missing one parameter"
     else
       db.del "metrics:#{id}:#{timestamp}", (err) ->
         if err
           callback err
         else
           callback null
