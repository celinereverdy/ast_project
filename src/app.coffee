express = require 'express'
bodyparser = require 'body-parser'
morgan = require 'morgan'
session = require 'express-session'
SessionStore = require('level-session-store')(session)

app = express()

server = require('http').Server(app)
io = require('socket.io')(server)

db = require './db'

metrics = require('./metrics')(db)
user = require('./user')(db)

#array socket
sockets = []

io.on 'connection', (socket) ->
  sockets.push socket

# Logging middleware
logging_middleware = (req, res, next) ->
  console.log 'logging'
  for socket, i in sockets
    socket.emit 'logs',
      username: if req.session.loggedIn then req.session.username else 'anonymous'
      url: req.url
  next()


app.set 'port', '8888'
app.set 'views', "#{__dirname}/../views"
app.set 'view engine', 'pug'

## Middleware
app.use bodyparser.json()
app.use bodyparser.urlencoded()
app.use morgan 'dev'
app.use session
  secret: "ast_secret"
  store: new SessionStore './db/sessions'
  resave: true
  saveUnitialized: true

app.use logging_middleware

app.use '/', express.static "#{__dirname}/../public"


## Authorization functions

authCheck = (req, res, next) ->
  unless req.session.loggedIn == true
    res.redirect '/login'
  else next()

noAuthCheck = (req, res, next) ->
  unless req.session.loggedIn != true
    res.redirect '/'
  else next()

## Authentication

# Render login page
app.get '/login', noAuthCheck, (req, res) ->
  res.render 'login'

# Log in a user
app.post '/login', noAuthCheck, (req, res) ->
  user.get req.body.username, (err, user) ->
    if err
      # if the user does not exist
      if err.notFound
        console.log "user not found"
        res.redirect '/login'
      # if there is another error
      else
        throw err
    # if the user exists
    else
      # if right password
      if req.body.password == user.password
         req.session.loggedIn = true
         req.session.username = user.username
         res.redirect '/'
      # if wrong password
      else
         console.log "wrong password"
         res.redirect '/login'

# Logout
app.get '/logout', authCheck, (req, res) ->
  delete req.session.loggedIn
  delete req.session.username
  res.redirect '/login'

# Render index page for logged in users
app.get '/', authCheck, (req, res) ->
  res.render 'index', name: req.session.username


## Metrics Router
metrics_router = express.Router()
metrics_router.use authCheck

# Get metric
metrics_router.get '/', (req, res) ->
  metrics.get req.session.username, (err, data) ->
    throw next err if err
    res.status(200).json data

# Save metric
metrics_router.post '/', (req, res) ->
  metric = [ timestamp: req.body.timestamp, value: req.body.value ]
  metrics.save req.session.username, metric, (err) ->
    throw next err if err
    #res.status(200).send 'metrics saved'
    console.log 'metrics saved'
    res.redirect '/'

# Remove a metric
metrics_router.delete '/:id/:timestamp', (req, res) ->
  metrics.remove req.params.id, req.params.timestamp, (err) ->
    throw next err if err
    res.status(200).send "metric removed"

app.use '/metrics', metrics_router


# Save a user
app.post '/user', (req, res) ->
  { username, password, email } = req.body
  user.save username, password, email, (err) ->
    throw next err if err
    res.status(200).send "user saved"

# Remove a user
app.delete '/user/:username', (req, res) ->
  user.remove req.params.username, (err) ->
    throw next err if err
    res.status(200).send "user removed"


server.listen app.get('port'), () ->
  console.log "Server listening on #{app.get 'port'} !"
