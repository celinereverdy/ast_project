module.exports = (db) ->
  # get(username, callback)
  # Get user
  # - username: user's username
  # - callback: the callback function
  get: (username, callback) ->
    if callback == undefined
      callback = username
      callback new Error "missing one parameter"
    else
      db.get "user:#{username}", (err, data) ->
        #throw err if err
        if err
           callback err
        else
           [ password, email ] = data.split ":"
           result =
             username: username
             password: password
             email: email
           callback null, result

  # save(username, password, email, callback)
  # Save given user
  # - username: user's username
  # - password: user's password
  # - email: user's email
  # - callback: the callback function
  save: (username, password, email, callback) ->
    if callback == undefined
      if email == undefined
        if password == undefined
          callback = username
          callback new Error "missing three parameters"
        else
          callback = password
          callback new Error "missing two parameters"
      else
        callback = email
        callback new Error "missing one parameter"
    else
      ws = db.createWriteStream()
      ws.on 'error', callback
      ws.on 'close', callback
      ws.write
        key: "user:#{username}"
        value: "#{password}:#{email}"
      ws.end()

  # remove(username, callback)
  # Remove user
  # - username: user's username
  # - callback: the callback function
  remove: (username, callback) ->
    if callback == undefined
      callback = username
      callback new Error "missing one parameter"
    else
      db.del "user:#{username}", (err) ->
        if err
           callback err
        else
           callback null
