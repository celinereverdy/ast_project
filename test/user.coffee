{exec} = require 'child_process'
should = require 'should'

describe "user", () ->

  user = null
  before (next) ->
      db = require '../src/db'
      user = require('../src/user')(db)
      next()

  describe "save", () ->

    it "save a user properly", (next) ->
      user.save 'username', 'password', 'email' , (err) ->
        should.not.exist err
        next()

    it "don't save a user because one missing parameter", (next) ->
      user.save 'username', 'password', (err) ->
        should.exist err
        next()

    it "don't save a user because two missing parameters", (next) ->
      user.save 'username', (err) ->
        should.exist err
        next()

    it "don't save a user because three missing parameters", (next) ->
      user.save (err) ->
        should.exist err
        next()

  describe "get", () ->

    it "get a user properly", (next) ->
      ## Create dummy data to then get
      user.save 'username', 'password', 'email', (err) ->
        return next err if err
        user.get 'username', (err, user) ->
          should.not.exist err
          next()

    it "don't save a user because one missing parameter", (next) ->
      ## Create dummy data to then get
      user.save 'username', 'password', 'email', (err) ->
        return next err if err
        user.get (err, user) ->
          should.exist err
          next()

    it "user is not found", (next) ->
      ## Create dummy data to then get
      user.save 'username', 'password', 'email', (err) ->
        return next err if err
        user.get 'fake_username', (err, user) ->
          should.exist err.notFound
          next()

  describe "remove", () ->

    it "remove a user properly", (next) ->
      ## Create dummy data to then remove and try to get
      user.save 'username', 'password', 'email', (err) ->
        return next err if err
        user.remove 'username', (err) ->
          should.not.exist err
          user.get 'username', (err, user) ->
            should.exist err.notFound
          next()

    it "don't remove a user because one missing parameter", (next) ->
      ## Create dummy data to then remove
      user.save 'username', 'password', 'email', (err) ->
        return next err if err
        user.remove (err) ->
          should.exist err
          next()
