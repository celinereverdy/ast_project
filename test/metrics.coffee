{exec} = require 'child_process'
should = require 'should'

describe "metrics", () ->

  metrics = null
  before (next) ->
    exec "rm -rf #{__dirname}/../db/*", (err, stdout) ->
      db = require '../src/db'
      metrics = require('../src/metrics')(db)
      next err

  describe "save", () ->

    it "save a metric properly", (next) ->
      metrics.save 'user', [
        timestamp:(new Date '2015-11-04 14:00 UTC').getTime(), value:10
       ,
        timestamp:(new Date '2015-11-04 14:10 UTC').getTime(), value:20
      ], (err) ->
        should.not.exist err
        next()

    it "don't save a metric because one missing parameter", (next) ->
      metrics.save 'user', (err) ->
        should.exist err
        next()

     it "don't save a metric because two missing parameters", (next) ->
       metrics.save (err) ->
         should.exist err
         next()

  describe "get", () ->

    it "get a metric properly", (next) ->
      ## Create dummy data to then get
      metrics.save 'user', [
        timestamp:(new Date '2015-11-04 14:00 UTC').getTime(), value:10
       ,
        timestamp:(new Date '2015-11-04 14:10 UTC').getTime(), value:20
      ], (err) ->
        return next err if err
        metrics.get 'user', (err, metrics) ->
          should.not.exist err
          next()

    it "don't get a metric because one missing parameter", (next) ->
      ## Create dummy data to then get
      metrics.save 'user', [
        timestamp:(new Date '2015-11-04 14:00 UTC').getTime(), value:10
       ,
        timestamp:(new Date '2015-11-04 14:10 UTC').getTime(), value:20
      ], (err) ->
        return next err if err
        metrics.get (err, metrics) ->
          should.exist err
          next()

    it "get empty metric because user does not exist", (next) ->
      ## Create dummy data to then get
      metrics.save 'user', [
        timestamp:(new Date '2015-11-04 14:00 UTC').getTime(), value:10
       ,
        timestamp:(new Date '2015-11-04 14:10 UTC').getTime(), value:20
      ], (err) ->
        return next err if err
        metrics.get 'user2', (err, metrics) ->
          metrics.should.be.empty
          next()

  describe "remove", () ->

    it "remove a metric properly", (next) ->
      ## Create dummy data to then remove
      metrics.save 'user', [
        timestamp:(new Date '2015-11-04 14:00 UTC').getTime(), value:10
       ,
        timestamp:(new Date '2015-11-04 14:10 UTC').getTime(), value:20
      ], (err) ->
        return next err if err
        metrics.remove 'user', (new Date '2015-11-04 14:00 UTC').getTime(), (err) ->
          should.not.exist err
          next()

    it "don't remove a user because one missing parameter", (next) ->
      ## Create dummy data to then remove
      metrics.save 'user', [
        timestamp:(new Date '2015-11-04 14:00 UTC').getTime(), value:10
       ,
        timestamp:(new Date '2015-11-04 14:10 UTC').getTime(), value:20
      ], (err) ->
        return next err if err
        metrics.remove 'user', (err) ->
          should.exist err
          next()

    it "don't remove a user because two missing parameters", (next) ->
      ## Create dummy data to then remove
      metrics.save 'user', [
        timestamp:(new Date '2015-11-04 14:00 UTC').getTime(), value:10
       ,
        timestamp:(new Date '2015-11-04 14:10 UTC').getTime(), value:20
      ], (err) ->
        return next err if err
        metrics.remove (err) ->
          should.exist err
          next()
